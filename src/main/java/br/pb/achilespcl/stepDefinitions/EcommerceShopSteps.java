package br.pb.achilespcl.stepDefinitions;

import br.pb.achilespcl.core.Properties;
import br.pb.achilespcl.page.EcommerceCartPage;
import br.pb.achilespcl.page.EcommerceHomePage;
import br.pb.achilespcl.page.EcommerceProductInfoPage;
import br.pb.achilespcl.page.EcommerceProductListPage;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.PageFactory;

import java.math.BigDecimal;
import java.util.Random;

import static br.pb.achilespcl.core.DriverFactory.getDriver;
import static br.pb.achilespcl.core.DriverFactory.killDriver;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class EcommerceShopSteps {

    public EcommerceHomePage ecommerceHomePage;
    public EcommerceProductListPage ecommerceProductListPage;
    public EcommerceProductInfoPage ecommerceProductInfoPage;
    public EcommerceCartPage ecommerceCartPage;
    public String productName;
    public BigDecimal totalCartPrice;

    @Before
    public void init() {
        productName="";
        ecommerceHomePage = PageFactory.initElements(getDriver(), EcommerceHomePage.class);
        ecommerceProductListPage = PageFactory.initElements(getDriver(), EcommerceProductListPage.class);
        ecommerceProductInfoPage = PageFactory.initElements(getDriver(), EcommerceProductInfoPage.class);
        ecommerceCartPage = PageFactory.initElements(getDriver(), EcommerceCartPage.class);
    }

    @After
    public void takeScreenshot(Scenario scenario) {
        final byte[] screenshot = ((TakesScreenshot) getDriver())
                .getScreenshotAs(OutputType.BYTES);
        scenario.embed(screenshot, "image/png");

        if (Properties.CLOSE_BROWSER) {
            killDriver();
        }
    }

    @Given("I am on home page")
    public void i_am_on_home_page() {
        ecommerceHomePage.load();
    }

    @Given("I search the product {string}")
    public void i_search_the_product(String productName) {
        this.productName = productName;
        ecommerceHomePage.search(productName);
    }

    @Given("I randomly select a product in the list")
    public void i_randoomly_select_a_product_in_the_list() {
        int randomProductIndex = new Random().nextInt(ecommerceProductListPage.getProductItems().size());
        ecommerceProductListPage.selectRandomProduct(randomProductIndex);
    }

    @When("I see a the product info")
    public void i_see_a_the_product_info() {
        assertTrue(ecommerceProductInfoPage.getProductName().toLowerCase().contains(productName.toLowerCase()));
    }

    @When("I click on 'buy' button")
    public void i_click_on_button() {
        ecommerceProductInfoPage.addProductToCart();
    }

    @Then("I see the correct product in my cart.")
    public void i_see_the_correct_product_in_my_cart() {
        assertEquals("1", ecommerceCartPage.getProductQuantity());
        assertTrue(ecommerceCartPage.getProductName().toLowerCase().contains(productName.toLowerCase()));
    }

    @Then("I see a error message containing {string}.")
    public void i_see_a_error_message_containing(String errorMsg) {
        assertTrue(ecommerceProductListPage.checktNotFoundMsg(errorMsg));
    }

    @Given("the product quantity in my cart equals to {int}")
    public void the_product_quantity_in_my_cart_equals_to(Integer quantity) {
        totalCartPrice = ecommerceCartPage.getTotalCartPrice();
        assertEquals(String.valueOf(quantity), ecommerceCartPage.getProductQuantity());
    }

    @When("I select a quantity equals to {int}")
    public void i_select_a_quantity_equals_to(Integer new_quantity) {
        ecommerceCartPage.selectQuantity(new_quantity);
    }

    @Then("the total amount increases {int} times.")
    public void the_total_amount_increases_times(Integer times) {
        assertEquals(totalCartPrice.multiply(new BigDecimal(String.valueOf(times))).setScale(2),
                ecommerceCartPage.getTotalCartPrice());
    }

    @Given("I have a product in my cart")
    public void i_have_a_product_in_my_cart() {
        assertEquals("1", ecommerceCartPage.getProductQuantity());
    }

    @When("I remove the product")
    public void i_remove_the_product() {
        ecommerceCartPage.removeProduct();
    }

    @Then("I see the message {string}")
    public void i_see_the_message(String message) {
        assertTrue(ecommerceCartPage.isEmptyCartWithMessage(message));
    }

}
