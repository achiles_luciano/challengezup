package br.pb.achilespcl.runners;

import br.pb.achilespcl.core.DriverFactory;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
		glue= {"br.pb.achilespcl.stepDefinitions"},
		features= {"classpath:features"},
		plugin = {"html:target/cucumber"},
		monochrome = true,
		dryRun = false)
public class RunnerEcommerceTest {
}
