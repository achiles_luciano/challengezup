package br.pb.achilespcl.page;

import br.pb.achilespcl.core.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EcommerceProductInfoPage extends BasePage {

    @FindBy(id = "product-name-default")
    private WebElement productName;

    @FindBy(id = "btn-buy")
    private WebElement productBuyBtn;

    @FindBy(id = "btn-continue")
    private WebElement productContinueToCart;

    @FindBy(xpath = "//*[text()[contains(.,'Sim, continuar')]]")
    private WebElement choiceTypeBtn;

    public String getProductName() {
        dsl.waitElementToBeVisible(productName, 3);
        return productName.getText();
    }

    public void addProductToCart() {
        productBuyBtn.click();

        if(dsl.checkElementPresence(choiceTypeBtn)) {
            choiceTypeBtn.click();
        }

        if (dsl.checkElementPresence(productContinueToCart)) {
            productContinueToCart.click();
        }
    }
}
