package br.pb.achilespcl.page;

import br.pb.achilespcl.core.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class EcommerceProductListPage extends BasePage {

    @FindBy(className = "product-grid")
    private WebElement productList;

    @FindAll({@FindBy(className = "product-grid-item")})
    private List<WebElement> productItems;

    public List<WebElement> getProductItems() {
        dsl.waitElementToBeVisible(productList, 3);
        return productItems;
    }

    public void selectRandomProduct(int index) {
        productItems.get(index).click();
    }

	public boolean checktNotFoundMsg(String errorMsg) {
    	return dsl.checkElementPresence(dsl.findByText(errorMsg));
	}
}
