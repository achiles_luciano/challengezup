package br.pb.achilespcl.page;

import br.pb.achilespcl.core.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EcommerceHomePage extends BasePage {

    @FindBy(id = "h_search-input")
    private WebElement searchField;

    @FindBy(id = "h_search-btn")
    private WebElement searchBtn;

    public void load() {
        dsl.load("/");
        dsl.waitElementToBeVisible(searchField, 3);
    }

    public void search(String productName) {
        searchField.sendKeys(productName);
        searchBtn.click();
    }
}
