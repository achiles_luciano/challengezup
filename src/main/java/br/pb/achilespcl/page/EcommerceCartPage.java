package br.pb.achilespcl.page;

import br.pb.achilespcl.core.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class EcommerceCartPage extends BasePage {

    @FindBy(className = "basket-productTitle")
    private WebElement productName;

    @FindBy(className = "select__quantity")
    private WebElement productQuantity;

    @FindBy(xpath = "//h2[text()[contains(.,'Sua cesta está vazia')]]")
    private WebElement emptyCartMsg;

    @FindBy(className = "summary-detail")
    private WebElement totalCart;

    @FindBy(className = "basket-productRemoveAct")
    private WebElement removeProductLink;

    public String getProductQuantity() {
        dsl.waitElementToBeVisible(productQuantity, 3);
        return dsl.getSelected(productQuantity);
    }

    public String getProductName() {
        return productName.findElement(By.tagName("a")).getAttribute("title");
    }

    public void selectQuantity(Integer new_quantity) {
        dsl.waitElementToBeVisible(productQuantity, 3);
        dsl.comboSelect(productQuantity, String.valueOf(new_quantity));
    }

    public BigDecimal getTotalCartPrice() {
        dsl.forceWait(2);
        String value = totalCart.getText().split("R\\$")[1].trim().replaceAll("\\D", "");
        return new BigDecimal(value).divide(new BigDecimal(100), 2, RoundingMode.CEILING);
    }

    public void removeProduct() {
        removeProductLink.click();
    }

    public boolean isEmptyCartWithMessage(String message) {
        dsl.waitElementToBeVisible(emptyCartMsg, 2);
        if(dsl.checkElementPresence(emptyCartMsg)) {
            return true;
        }
        return false;
    }
}
