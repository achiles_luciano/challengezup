package br.pb.achilespcl.core;
import static br.pb.achilespcl.core.DriverFactory.getDriver;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


public class DSL {
	
	public void load(String path) {
		getDriver().get(Properties.ENDPOINT + path);
	}

	public void loadFullPath(String path) {
		getDriver().get(path);
	}

	public WebElement findById(String _id){
		return getDriver().findElement(By.id(_id));
	}
	
	public WebElement findByTag(String tagName){
		return getDriver().findElement(By.tagName(tagName));
	}
	
	public WebElement findByClass(String className){
		return getDriver().findElement(By.className(className));
	}

	public List<WebElement> findAllByClass(String className){
		return getDriver().findElements(By.className(className));
	}

	public WebElement findByXpath(String xpath){
		return getDriver().findElement(By.xpath(xpath));
	}

	public void write(By locator, String text){
		getDriver().findElement(locator).sendKeys(text);
	}
	
	public void writeMaskField(By locator, String text){
		getDriver().findElement(locator).sendKeys(Keys.HOME + text);
	}
	
	public String getValue(String _id){
		return findById(_id).getAttribute("value");
	}
	
	public String getText(By locator){
		return getDriver().findElement(locator).getText();
	}
	
	public String getTextByXpath(String xpath){
		return findByXpath(xpath).getText();
	}
	
	public void click(String _id){
		findById(_id).click();
	}
	
	public boolean isChecked(String _id){
		return findById(_id).isSelected();
	}
	
	public void comboSelect(String _id, String search){
		WebElement element = findById(_id);
		Select combo = new Select(element);
		combo.selectByVisibleText(search);	
	}

	public void comboSelect(WebElement element, String option){
		Select combo = new Select(element);
		combo.selectByVisibleText(option);
	}
	
	public String getSelected(String _id){
		WebElement element = findById(_id);
		Select combo = new Select(element);
		return combo.getFirstSelectedOption().getText();
	}

	public String getSelected(WebElement element){
		Select combo = new Select(element);
		return combo.getFirstSelectedOption().getText();
	}

	public void goToLink(String link){
		getDriver().findElement(By.linkText(link)).click();
	}
	
	public void waitElement(By locator, int time){
		WebDriverWait wait = new WebDriverWait(getDriver(), time);
		wait.until(ExpectedConditions.presenceOfElementLocated(locator));
	}

	public void waitElementClickable(WebElement element, int time) {
		WebDriverWait wait = new WebDriverWait(getDriver(), time);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void waitElementToBeVisible(WebElement element, int time){
		WebDriverWait wait = new WebDriverWait(getDriver(), time);
		wait.until(ExpectedConditions.visibilityOf(element));
	}

	public void waitElementToBeVisible(By locator, int time){
		WebDriverWait wait = new WebDriverWait(getDriver(), time);
		wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
	}

	public void scrollPageToElement(WebElement scrollElement) {
		waitElement(By.className("big"), 3);
		ArrayList<String> list = new ArrayList<String>(getDriver().getWindowHandles());
		getDriver().switchTo().window(list.get(1));
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		boolean isFinished = false;
		while (!isFinished) {
			try {
				js.executeScript("window.scrollBy(0, window.outerHeight)", "");
				waitElementToBeVisible(scrollElement, 1);
				isFinished = true;
			} catch (Exception e) {
				System.out.println("scrolling...");
			}
		}
	}

	public void scrollPage(int horizontal, int vertical) {
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		js.executeScript("window.scrollBy("+ horizontal +", "+ vertical +")", "");
	}
	
	public String checkUrl() {
		return getDriver().getCurrentUrl();
	}

	public WebElement findByText(String text) {
		return getDriver().findElement(By.xpath("//*[text()[contains(.,'"+ text +"')]]"));
	}

	public WebElement up(WebElement element, int times) {
		String path = "..";
		for (int t = 1; t < times; t++) {
			path += "/..";
		}
		return element.findElement(By.xpath(path));
	}

	public boolean checkElementPresence(WebElement element){
		try {
			if(element.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (NoSuchElementException nse) {
			return false;
		}
	}

	public void forceWait(int time) {
		try{
			Thread.sleep(time  * 1000);
		} catch (InterruptedException e) {
			System.out.println("timeout");
		}
	}
}
