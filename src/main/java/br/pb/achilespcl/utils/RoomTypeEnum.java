package br.pb.achilespcl.utils;

public enum RoomTypeEnum {
    INDIVIDUAL,
    DOUBLE,
    FAMILY,
    MULTIPLE
}
