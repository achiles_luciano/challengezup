Feature: As a user I want to change a product of on my cart

	Background: Insert a product on my cart successfully
		Given I am on home page
		And I search the product "Samsung Galaxy s10"
		And I randomly select a product in the list
		When I see a the product info
		And I click on 'buy' button
		Then I see the correct product in my cart.

	Scenario Outline: Add product quantity
		Given the product quantity in my cart equals to <quantity>
		When I select a quantity equals to <new_quantity>
		Then the total amount increases <new_quantity> times.
		Examples:
			| quantity | new_quantity |
			| 1	       | 2            |

	Scenario: Remove product from cart
		Given I have a product in my cart
		When I remove the product
		Then I see the message "sua cesta está vazia"

