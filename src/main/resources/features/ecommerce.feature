Feature: As a user I want to search a product and insert it on my cart

	Scenario: Insert a product on my cart successfully
		Given I am on home page
		And I search the product "Samsung Galaxy s10"
		And I randomly select a product in the list
		When I see a the product info
		And I click on 'buy' button
		Then I see the correct product in my cart.

	Scenario: Search for a inexistent product
		Given I am on home page
		When I search the product "testInexistentProductOk"
		Then I see a error message containing "nenhum resultado encontrado".

