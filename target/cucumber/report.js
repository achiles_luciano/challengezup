$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/ecommerce-cart.feature");
formatter.feature({
  "name": "As a user I want to change a product of on my cart",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "Add product quantity",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "the product quantity in my cart equals to \u003cquantity\u003e",
  "keyword": "Given "
});
formatter.step({
  "name": "I select a quantity equals to \u003cnew_quantity\u003e",
  "keyword": "When "
});
formatter.step({
  "name": "the total amount increases \u003cnew_quantity\u003e times.",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "quantity",
        "new_quantity"
      ]
    },
    {
      "cells": [
        "1",
        "2"
      ]
    }
  ]
});
formatter.background({
  "name": "Insert a product on my cart successfully",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I am on home page",
  "keyword": "Given "
});
formatter.match({
  "location": "EcommerceShopSteps.i_am_on_home_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I search the product \"Samsung Galaxy s10\"",
  "keyword": "And "
});
formatter.match({
  "location": "EcommerceShopSteps.i_search_the_product(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I randomly select a product in the list",
  "keyword": "And "
});
formatter.match({
  "location": "EcommerceShopSteps.i_randoomly_select_a_product_in_the_list()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I see a the product info",
  "keyword": "When "
});
formatter.match({
  "location": "EcommerceShopSteps.i_see_a_the_product_info()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on \u0027buy\u0027 button",
  "keyword": "And "
});
formatter.match({
  "location": "EcommerceShopSteps.i_click_on_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I see the correct product in my cart.",
  "keyword": "Then "
});
formatter.match({
  "location": "EcommerceShopSteps.i_see_the_correct_product_in_my_cart()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Add product quantity",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "the product quantity in my cart equals to 1",
  "keyword": "Given "
});
formatter.match({
  "location": "EcommerceShopSteps.the_product_quantity_in_my_cart_equals_to(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I select a quantity equals to 2",
  "keyword": "When "
});
formatter.match({
  "location": "EcommerceShopSteps.i_select_a_quantity_equals_to(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the total amount increases 2 times.",
  "keyword": "Then "
});
formatter.match({
  "location": "EcommerceShopSteps.the_total_amount_increases_times(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.embedding("image/png", "embedded0.png");
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "Insert a product on my cart successfully",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I am on home page",
  "keyword": "Given "
});
formatter.match({
  "location": "EcommerceShopSteps.i_am_on_home_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I search the product \"Samsung Galaxy s10\"",
  "keyword": "And "
});
formatter.match({
  "location": "EcommerceShopSteps.i_search_the_product(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I randomly select a product in the list",
  "keyword": "And "
});
formatter.match({
  "location": "EcommerceShopSteps.i_randoomly_select_a_product_in_the_list()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I see a the product info",
  "keyword": "When "
});
formatter.match({
  "location": "EcommerceShopSteps.i_see_a_the_product_info()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on \u0027buy\u0027 button",
  "keyword": "And "
});
formatter.match({
  "location": "EcommerceShopSteps.i_click_on_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I see the correct product in my cart.",
  "keyword": "Then "
});
formatter.match({
  "location": "EcommerceShopSteps.i_see_the_correct_product_in_my_cart()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Remove product from cart",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "I have a product in my cart",
  "keyword": "Given "
});
formatter.match({
  "location": "EcommerceShopSteps.i_have_a_product_in_my_cart()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I remove the product",
  "keyword": "When "
});
formatter.match({
  "location": "EcommerceShopSteps.i_remove_the_product()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I see the message \"sua cesta está vazia\"",
  "keyword": "Then "
});
formatter.match({
  "location": "EcommerceShopSteps.i_see_the_message(String)"
});
formatter.result({
  "status": "passed"
});
formatter.embedding("image/png", "embedded1.png");
formatter.after({
  "status": "passed"
});
formatter.uri("features/ecommerce.feature");
formatter.feature({
  "name": "As a user I want to search a product and insert it on my cart",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Insert a product on my cart successfully",
  "description": "",
  "keyword": "Scenario"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I am on home page",
  "keyword": "Given "
});
formatter.match({
  "location": "EcommerceShopSteps.i_am_on_home_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I search the product \"Samsung Galaxy s10\"",
  "keyword": "And "
});
formatter.match({
  "location": "EcommerceShopSteps.i_search_the_product(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I randomly select a product in the list",
  "keyword": "And "
});
formatter.match({
  "location": "EcommerceShopSteps.i_randoomly_select_a_product_in_the_list()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I see a the product info",
  "keyword": "When "
});
formatter.match({
  "location": "EcommerceShopSteps.i_see_a_the_product_info()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on \u0027buy\u0027 button",
  "keyword": "And "
});
formatter.match({
  "location": "EcommerceShopSteps.i_click_on_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I see the correct product in my cart.",
  "keyword": "Then "
});
formatter.match({
  "location": "EcommerceShopSteps.i_see_the_correct_product_in_my_cart()"
});
formatter.result({
  "status": "passed"
});
formatter.embedding("image/png", "embedded2.png");
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Search for a inexistent product",
  "description": "",
  "keyword": "Scenario"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I am on home page",
  "keyword": "Given "
});
formatter.match({
  "location": "EcommerceShopSteps.i_am_on_home_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I search the product \"testInexistentProductOk\"",
  "keyword": "When "
});
formatter.match({
  "location": "EcommerceShopSteps.i_search_the_product(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I see a error message containing \"nenhum resultado encontrado\".",
  "keyword": "Then "
});
formatter.match({
  "location": "EcommerceShopSteps.i_see_a_error_message_containing(String)"
});
formatter.result({
  "status": "passed"
});
formatter.embedding("image/png", "embedded3.png");
formatter.after({
  "status": "passed"
});
});